{-# OPTIONS -Werror=incomplete-patterns #-}

{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE PolyKinds       #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators   #-}

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Map.Strict     as Map
import           Data.Proxy          (Proxy (..))
import           Hedgehog            (Property, checkParallel, discover,
                                      property, (===))

import           Pair

prop_use_in_associasions_list :: Property
prop_use_in_associasions_list =
  property $
    ["charlie" := abs (44 + 44), "bravo" := 43, "alpha" := 42]
    === ( [("charlie", abs $ 44 + 44), ("bravo", 43), ("alpha", 42)]
          :: [(String, Int)]
        )

prop_use_in_map :: Property
prop_use_in_map =
  property $
    ["charlie" := abs (44 + 44 :: Int), "bravo" := 43, "alpha" := 42]
    === Map.fromList [("charlie", abs $ 44 + 44), ("bravo", 43), ("alpha", 42)]

prop_use_in_hashmap :: Property
prop_use_in_hashmap =
  property $
    ["charlie" := abs (44 + 44 :: Int), "bravo" := 43, "alpha" := 42]
    === HashMap.fromList
          [("charlie", abs $ 44 + 44), ("bravo", 43), ("alpha", 42)]

test_match_complete :: (a, b) -> ()
test_match_complete (_ := _) = ()

test_typelevel_polykinds :: Proxy [True := "hello", False := "world"]
test_typelevel_polykinds = Proxy

main :: IO ()
main = do
  True <- checkParallel $$discover
  pure ()
  where
    _ = test_match_complete
    _ = test_typelevel_polykinds
