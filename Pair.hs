{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PolyKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Pair where

pattern (:=) :: a -> b -> (a, b)
pattern a := b = (a, b)
infix 0 :=
{-# COMPLETE (:=) #-}

type a := b = '(a, b)
